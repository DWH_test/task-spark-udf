# Тестовое задание Spark

Задание выполняется на кластере Spark.

Представь, что ты проектируешь систему. 
Способ доставки данных - потоковые данные
Объём данных - 5 млн строк в день
Тип аналитических запросов - обучение ml-модели
Семпл данных приведён в репозитории. Формат - Json

Необходимо разработать аритектуру для корректного использования данных
